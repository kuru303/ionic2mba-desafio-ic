import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ChatPage } from '../pages/chat/chat';
import { PerfilPage } from '../pages/perfil/perfil';
import { UsuarioProvider } from '../pages/provider/usuarioProvider';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ChatPage,
    PerfilPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ChatPage,
    PerfilPage
  ],
  providers: [UsuarioProvider]
})
export class AppModule {}
