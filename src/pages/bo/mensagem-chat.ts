export class MensagemChat {
  userName: String;
  message: String;
  centralPosition: String;
  isMapa: Boolean;
  classe: String;
  lang: String;
}