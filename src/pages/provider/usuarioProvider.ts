import { Injectable } from '@angular/core';
import {NativeStorage, Device} from 'ionic-native';
import { UsuarioLocal } from '../../pages/bo/usuario';

//ionic plugin add cordova-plugin-nativestorage
//ionic plugin add cordova-plugin-device
@Injectable()
export class UsuarioProvider {
    private usuarioLocal: UsuarioLocal;
    public defaultUserName : string;

    constructor(){
        //this.defaultUserName = 'User('+Device.device.uuid+')';
        this.defaultUserName = 'UserDefault';
        this.getUsuarioDefault();
    }

    getUsuarioLocal(): UsuarioLocal {
        console.log('pegando o usuario : ' + this.usuarioLocal.nome);
        if ( !this.usuarioLocal ) {
            console.log(' Recuperando do storage ');
            NativeStorage.getItem('myUser')
                .then(data => {
                                console.log(data);
                                this.usuarioLocal=data
                              },
                      error => {
                                 console.error(error)
                                 this.getUsuarioDefault();
                               } 
            );

        }

        return this.usuarioLocal;
    }

    setUsuarioLocal(usuarioLocal: UsuarioLocal){
        this.usuarioLocal = usuarioLocal;
        NativeStorage.remove('myUser');
        NativeStorage.setItem('myUser', usuarioLocal)
            .then( () => console.log('Stored item!'),
                error => console.error('Error storing item', error)
        );

    }


    private getUsuarioDefault(){
        this.usuarioLocal=new UsuarioLocal;
        this.usuarioLocal.nome = this.defaultUserName;
        this.usuarioLocal.foto = 'assets/images/marvin-profile.png';
    }


}