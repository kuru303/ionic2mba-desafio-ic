import { Component, NgZone, ViewChild } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import io from 'socket.io-client';
import { NavController, Tabs } from 'ionic-angular';
import { MensagemChat } from '../../pages/bo/mensagem-chat';
import { UsuarioLocal } from '../../pages/bo/usuario';
import { UsuarioProvider } from '../../pages/provider/usuarioProvider';
import { Geolocation } from 'ionic-native';
import { Content } from 'ionic-angular';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})

//npm install socket.io-client --save
//typings install dt~socket.io-client --save --global
//npm install @types/socket.io-client --save-dev --save-exact
//ionic plugin add cordova-plugin-geolocation
export class ChatPage {
  tab:Tabs;
  @ViewChild(Content) content: Content;

  socket: SocketIOClient.Socket;
  msgChat: string;
  chats: MensagemChat[];
  zone: NgZone;
  posicaoGps: string = '';
  mensagem: MensagemChat;
  fotoMsg: String;
  estiloLocalRemoto: String;

  constructor(public navCtrl: NavController, private usuarioProvider: UsuarioProvider, private http:Http) {
    console.log('Construtor chat');
    console.log('Usuario atual >>' + this.usuarioProvider.getUsuarioLocal().nome);

    this.zone = new NgZone({enableLongStackTrace: false});
    this.msgChat = '';
    this.chats = [];

    this.socket = io('http://10.0.1.9:3000/', { query: 'user='+this.usuarioProvider.getUsuarioLocal().nome });
    this.socket.on('message', (msg: MensagemChat) => {
            this.zone.run(() => {
                this.chats.push(msg);
                this.scrollToBottom();
            });
        });
    }
    
    send(msg: string, isMapa: boolean) {
        if(msg != ''){
            this.mensagem = {
                userName: this.usuarioProvider.getUsuarioLocal().nome, 
                message: msg, 
                centralPosition: this.posicaoGps, 
                isMapa: isMapa,
                classe: this.getClasseEstilo(),
                lang: this.usuarioProvider.getUsuarioLocal().lang
            };
            this.socket.emit('message', this.mensagem);
            this.mensagem = null;
        }
        this.msgChat = '';
    }

    sendLocation(){
        Geolocation.getCurrentPosition().then((resp) => {
            this.posicaoGps = resp.coords.latitude +  "," + resp.coords.longitude;
            this.send(this.getMapa(this.posicaoGps), true); 
            this.posicaoGps = '';
        }).catch((error) => {
        console.log('Error getting location', error);
        });
    }

    getMapa(central) : string {
        return 'https://maps.googleapis.com/maps/api/staticmap?center=' + central + '&zoom=16&size=300x200&maptype=roadmap&markers=color:red%7Clabel:C%7C' + central + '&key=AIzaSyBGKN-vGroNOxqZ7qoENiv-VyMQpsKPfX4';
    }

    scrollToBottom() {
        this.content.scrollToBottom();
    }

    getClasseEstilo() : String {
        if ( this.usuarioProvider.getUsuarioLocal().nome.startsWith("#") ) {
            return "message-remote";
        }
        return "message-local";
    }

}
