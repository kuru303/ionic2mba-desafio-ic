import { Component, NgZone } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Camera, NativeStorage} from 'ionic-native';
import { UsuarioLocal } from '../../pages/bo/usuario';
import { UsuarioProvider } from '../../pages/provider/usuarioProvider';
 
//ionic plugin add cordova-plugin-camera
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html'
})

export class PerfilPage {

    base64Foto: string;
    nomePerfil: string;
    usuarioLocal: UsuarioLocal;
    lang: string = "pt";

    constructor(public navCtrl: NavController, private usuarioProvider: UsuarioProvider) {
        console.log('Construtor chat');
        
        this.base64Foto = 'assets/images/marvin-profile.png';
        this.nomePerfil = '';

        if ( this.usuarioProvider.getUsuarioLocal().nome != this.usuarioProvider.defaultUserName ) {
            this.nomePerfil = this.usuarioProvider.getUsuarioLocal().nome;
            this.base64Foto = this.usuarioProvider.getUsuarioLocal().foto;
        }    
    }

    takePicture() {
        console.log('...SORRIA ....');

        Camera.getPicture({
            quality : 75,
            destinationType : Camera.DestinationType.DATA_URL,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : true,
            encodingType: Camera.EncodingType.PNG,
            targetWidth: 200,
            targetHeight: 200,
            saveToPhotoAlbum: false
        }).then(imageData => {
            this.base64Foto = "data:image/jpg;base64," + imageData;
        }, error => {
            console.log("ERROR -> " + JSON.stringify(error));
        });
    }

    ionViewWillLeave(){
        this.usuarioLocal = new UsuarioLocal;
        this.usuarioLocal.foto = this.base64Foto;
        if ( this.nomePerfil == '' ){
            this.nomePerfil = this.usuarioProvider.defaultUserName
        }
        this.usuarioLocal.nome = this.nomePerfil;
        this.usuarioLocal.lang = this.lang;
        this.usuarioProvider.setUsuarioLocal(this.usuarioLocal);
    }

}