import { Component, NgZone } from '@angular/core';
import { NavController, Nav, Tabs } from 'ionic-angular';

import io from 'socket.io-client';

// import Tabs
import { PerfilPage } from '../../pages/perfil/perfil';
import { ChatPage } from '../../pages/chat/chat';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  tab1: any;
  tab2: any;

  constructor(public navCtrl: NavController, private nav: Nav) {
    this.tab1 = PerfilPage;
    this.tab2 = ChatPage;
  }

}
