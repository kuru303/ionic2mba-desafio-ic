# Desafio MBAMobi - por Adriano Oliveira

## Descrição
Aplicativo desenvolvido, visando atender a todos os requisitos do desafio.

Para o desafio foram propostos os seguintes intens:
Desafio 1 - MBAcam
   O objetivo deste aplicativo é usar o recurso nativo câmera. O app deve poder tirar fotos ou escolher da galeria e exibir as fotos em uma lista. 
Desafio 2 - MBAMaps
   Este aplicativo deve permitir ao usuário consultar a localização atual do usuário, exibir a localização num mapa e conter um botão para abrir o ponto local no aplicativo nativo do usuário(Integrar com plugin Cordova)
Desafio 3 - App desafio
Escolher um dos 3 apps para implementar: 
   - Implementar um app que permita imprimir um PDF numa impressora Bluetooth
   - Implementar um app de chat em tempo real
   - Implementar um app que faça validação de certificado A1
Desafio 4
Implementar um app que ajude a resolver um problema da cidade

Assim foi criado um aplicativo que compusesse todos os requisitos.

O aplicativo é uma proposta de atendimento ao turista por intermédio de um chat instantâneo,
a partir do qual o usuário cadastra seu nome e coloca sua foto de perfil ( Desafio 1 ).
Feito isso, na aba de conversa - chat - ( Desafio 3 ), é aberto um diálogo.
Durante o diálogo o usuário pode enviar sua localização, utilizando o GPS ( Desafio 2 ).
Com essa proposta ficaria solucionado o atendimento remoto ao turista da cidade ( Desafio 4 ).

---------------------
## Instalação
Foram criados 2 projetos

###### Servidor: Node.Js
https://gitlab.com/kuru303/ionic2mba-desafio-serve

Baixe o projeto acesse a pagina e execute o index.js 
> node index.js 

Nesse momento o servidor é iniciado e o endereço ip desta máquina deverá ser usado pelos clientes.

###### Cliente: Ionic V2 RC01
https://gitlab.com/kuru303/ionic2mba-desafio-ic

Baixe o projeto e antes de fazer o build para execução, altere o arquivo src/pages/chat/chat.ts linha 42 e
coloque o IP de onde o servidor esta rodando.

----------------------
## Uso
Esse aplicativo tem uma proposta de chat "one-to-many", ou seja vários usuarios, os turistas, conversando 
com um 'super usuário', o atendente.

Para o uso do turista basta abrir o aplicativo, colocar um username (sem espaços) e iniciar a interação.
Ex.: TuristaAlfa

Agora, para a aplicação reconhecer que o usuario cadastrado é um super usuário, É NECESSÁRIO que seu user name
comece com o caracter '#'

Ex.: #Atendente

Desta forma, quando um ou mais turistas enviarem mensagem o Atendente irá recebê-las.
E por fim, para que a mensgem do atendente seja direcionada ao chat especifico do turista, o 
Atendente deve citar o usuário para qual esta enviando a mensagem através do caracter '@'

Exemplo mensgem do atendente para o turista:
> @TuristaAlfa Onde voce esta?


------------------------

### O aplicativo foi feito com intúito de atender aos requisitos do desafio e não pode ser considerado como
### uma versão final para comercialização.



